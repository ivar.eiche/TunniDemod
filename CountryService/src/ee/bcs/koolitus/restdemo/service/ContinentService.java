package ee.bcs.koolitus.restdemo.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.restdemo.bean.Continent;

public class ContinentService {

	public List<Continent> getAllContinents() {
		List<Continent> listOfContinents = new ArrayList<>();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement
						.executeQuery("SELECT * FROM country_management.continent;");) {
			while (resultSet.next()) {
				listOfContinents.add(new Continent(resultSet.getInt("continent_id"),
						resultSet.getString("name")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfContinents;
	}

	public Continent getContinentById(int continentId) {
		Continent continent = null;
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"SELECT * FROM country_management.continent WHERE continent_id="
								+ continentId + ";");) {
			while (resultSet.next()) {
				continent = new Continent(resultSet.getInt("continent_id"),
						resultSet.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return continent;
	}

	public Continent getContinentByName(String name) {
		Continent continent = null;
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"SELECT * FROM country_management.continent WHERE name='"
								+ name + "';");) {
			while (resultSet.next()) {
				continent = new Continent(resultSet.getInt("continent_id"),
						resultSet.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return continent;
	}
}
