package ee.bcs.koolitus.main;

import ee.bcs.koolitus.mammal.Human;
import ee.bcs.koolitus.mammal.Mammal;
import ee.bcs.koolitus.mammal.Student;

public class Main {

	public static void main(String[] args) {
		Mammal mammal = new Mammal("unknown");
		mammal.breathesWithLungs();
		
		Human human = new Human();
		human.breathesWithLungs();
		human.talk();
		
		Student student = new Student();
		student.breathesWithLungs();
		
		Mammal inimeneMammal = new Human();
		inimeneMammal.breathesWithLungs();
		//inimeneMammal.talk(); //ei saa, kuna mammalil ei ole seda meetodit
	}

}
