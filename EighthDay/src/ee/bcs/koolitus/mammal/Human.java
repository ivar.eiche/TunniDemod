package ee.bcs.koolitus.mammal;

public class Human extends Mammal {
	
	public Human(){
		super("Homo Sapiens");
	}
	
	@Override
	public void breathesWithLungs() {
		System.out.println("Human breathes through nose with lungs");
	}
	
	public void talk(){
		System.out.println("This is a sentence!");
	}
}
