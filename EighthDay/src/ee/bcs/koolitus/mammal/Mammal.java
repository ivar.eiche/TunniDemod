package ee.bcs.koolitus.mammal;

public class Mammal {
	private String speciesName = "";
	
	public Mammal(String speciesName){
		this.speciesName = speciesName;
		System.out.println(this.speciesName);
	}

	public void breathesWithLungs() {
		System.out.println("Mammal breathes with lungs");
	}

	public String getSpeciesName() {
		return speciesName;
	}

	public void setSpeciesName(String speciesName) {
		this.speciesName = speciesName;
	}

}
