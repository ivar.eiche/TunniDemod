package ee.bcs.koolitus.interfacedemo;

import java.awt.Point;

public interface IShape {
	
	void setCentreCoordinates(int x, int y);
}
