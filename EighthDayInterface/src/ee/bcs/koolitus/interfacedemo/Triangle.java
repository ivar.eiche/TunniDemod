package ee.bcs.koolitus.interfacedemo;

import java.awt.Point;

public class Triangle implements IShape {
	public Point centrePoint = new Point();

	@Override
	public void setCentreCoordinates(int x, int y) {
		centrePoint.setLocation(x, y);
	}

	public Point getCentrePoint() {
		return centrePoint;
	}

	public void setCentrePoint(Point centrePoint) {
		this.centrePoint = centrePoint;
	}
}
