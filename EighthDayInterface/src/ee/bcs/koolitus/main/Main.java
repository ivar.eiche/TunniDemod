package ee.bcs.koolitus.main;

import ee.bcs.koolitus.interfacedemo.IShape;
import ee.bcs.koolitus.interfacedemo.Triangle;

public class Main {

	public static void main(String[] args) {
		Triangle triangle  = new Triangle();
		triangle.setCentreCoordinates(2, 3);
		System.out.println(triangle.getCentrePoint());
	}

}
