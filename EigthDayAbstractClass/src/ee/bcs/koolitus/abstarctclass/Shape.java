package ee.bcs.koolitus.abstarctclass;

import java.awt.Point;

public abstract class Shape {
	public final String DEFAULT_COLOR = "green";
	private Point centrePoint = new Point();
	
	//tavaline meetod
	public void setCentreCoordinates(int x, int y){
		centrePoint.setLocation(x, y);
	}
	
	public abstract double getPerimeter();

	public Point getCentrePoint() {
		return centrePoint;
	}

	public void setCentrePoint(Point centrePoint) {
		this.centrePoint = centrePoint;
	}

}
