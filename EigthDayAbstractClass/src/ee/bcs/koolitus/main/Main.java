package ee.bcs.koolitus.main;

import ee.bcs.koolitus.abstarctclass.Shape;
import ee.bcs.koolitus.abstarctclass.Triangle;

public class Main {

	public static void main(String[] args) {
		Triangle triangle = new Triangle();
		System.out.println(triangle.DEFAULT_COLOR);
		triangle.setCentreCoordinates(1, 5);
		System.out.println(triangle.getCentrePoint());
		triangle.setSideALength(2.0);
		triangle.setSideBLength(3.0);
		triangle.setSideCLength(2.5);
		System.out.println(triangle.getPerimeter());
	}

}
