package ee.bcs.koolitus.fifthDay;

import java.util.Arrays;

public class CitiesInArray {
	public static void main(String[] args) {
		String[][] cities = new String[196][];
		cities[0] = new String[4];
		cities[0][0] = "Tallinn";
		cities[0][1] = "Tallinn";
		cities[0][2] = "Tallinn";
		cities[0][3] = "Eesti";
		cities[1] = new String[5];
		cities[1][0] = "Helsinki";
		cities[1][1] = "Helsingi";
		cities[1][2] = "Helsinki";
		cities[1][3] = "Helsingfors";
		cities[1][4] = "Soome";
		cities[2] = new String[4];
		cities[2][0] = "Riga";
		cities[2][1] = "Riia";
		cities[2][2] = "Riga";
		cities[2][3] = "Läti";
		System.out.println("--Sisestatud andmed-------------");
		System.out.println(Arrays.deepToString(cities));

		// Trükin ainult eesti keelsed nimed
		System.out.println("\n--Eesti keelsed nimed-----------");
		for (String[] city : cities) {
			if (city != null) {
				System.out.println(city[2]);
			} else {
				break;
			}
		}

		System.out.println("\n--Riik on esimeseks liigutatud-----");
		// muudan elementide järjekorda reas, nii et riik läheks esimeseks
		for (int j = 0; j < cities.length; j++) {
			if (cities[j] == null) {
				break;
			}
			String[] tempCountryCityRow = new String[cities[j].length];
			tempCountryCityRow[0] = cities[j][cities[j].length - 1];
			for (int i = 1; i < cities[j].length; i++) {
				tempCountryCityRow[i] = cities[j][i - 1];
			}
			cities[j] = tempCountryCityRow;
		}
		System.out.println(Arrays.deepToString(cities));

		System.out.println("\n--Riigi nimetuse ette sõna country:----");
		// pannen riigi nimedele ette sõna country:
		for (String[] city : cities) {
			if (city == null) {
				break;
			}
			city[0] = "country:" + city[0];
		}

		// trükin välja kõik riikide nimed
		for (String[] city : cities) {
			if (city == null) {
				break;
			}
			System.out.println(city[0]);
		}

		System.out.println("\n--Lausete trükkimine------");
		// trükin "Riik – riik: pealinn – eesti keelne nimi; inglise keeles –
		// inglise keelne nimi, kohalikus keeles – kohalikus keeles nimi"
		for (String[] countryCities : cities) {
			// kontrollin, et ei oleks tühi rida
			if (countryCities == null) {
				break;
			}
			String stateNamePhrase = countryCities[0];
			String[] stateNameAfterSplitArray = stateNamePhrase.split(":");
			// [0] = state [1] = stateName
			String stateName = stateNameAfterSplitArray[1];
			// trükin lause kuni kohalike nimedeni
			System.out.print("Riik – " + stateName + ": pealinn – "
					+ countryCities[2] + "; inglise keeles – "
					+ countryCities[1] + ", kohalikus keeles – ");
			// leian mitu veergu on reas
			int rowLength = countryCities.length;
			// võtan need veerud, mis tulevad alates esimesest kohaliku nime
			// veerust, st veerg 3 kuni rea pikkus (nb, indeks on ühe võrra
			// väiksem
			for (int localNames = 3; localNames < rowLength; localNames++) {

				if (localNames < rowLength - 1) {
					System.out.print(countryCities[localNames] + "; ");
				} else {
					System.out.print(countryCities[localNames]);
				}
			}
			System.out.println();
		}

		System.out.println("\n--Uue riigi lisamine kolmandale kohale-----");
		// Loon uue riigi objekti
		String[] kurrunurru = { "country:Kurrunurruvutisaare Kuningriik",
				"LongStocking City", "Pikksuka linn", "Langstrump" };
		// loend alates kolmandast reast olevad riigid ajutisse massiivi,
		// i = vanas tabelis asukoht, j = ajutisse tabelisse minev koht
		String[][] tempStates = new String[cities.length - 1][];
		for (int i = 2, j = 0; i < cities.length; i++, j++) {
			if (cities[i] == null) {
				break;
			}
			tempStates[j] = cities[i];
		}
		// panen vanasse tabelisse kolmandale reale uue linna
		cities[2] = kurrunurru;
		// panen ajutisest tabelist read peale uut rida
		for (int i = 0; i < tempStates.length; i++) {
			if (tempStates[i] == null) {
				break;
			}
			cities[i + 3] = tempStates[i];
		}
		System.out.println(Arrays.deepToString(cities));

		System.out.println(
				"\n--Kolmandal kohal oleva riigi eemaldamine, nii et tühja rida ei tekiks-----");
		String[][] tempStatesRemove = new String[cities.length - 1][];
		// loen eelnevad read ajutisse massiivi
		for (int i = 0; i < 2; i++) {
			if (cities[i] == null) {
				break;
			}
			tempStatesRemove[i] = cities[i];
		}
		// loen järgnevad read ajutisse massiivi
		for (int i = 3, j = 2; i < cities.length; i++, j++) {
			if (cities[i] == null) {
				break;
			}
			tempStatesRemove[j] = cities[i];
		}
		// asendan vana tabeli ajutisega
		cities = tempStatesRemove;
		System.out.println(Arrays.deepToString(cities));

		// Kui tühi rida oleks lubatud oleks vastava rea võinud lihtsalt
		// null-iks määrata:
		// cities[2] = null;
		// ja mingeid nihutamisi ajutise tabeli abil poleks vaja olnud
	}

}
