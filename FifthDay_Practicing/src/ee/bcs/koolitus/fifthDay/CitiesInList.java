package ee.bcs.koolitus.fifthDay;

import java.util.ArrayList;
import java.util.List;

public class CitiesInList {

	public static void main(String[] args) {
		List<List<String>> statesWithCities = new ArrayList<>();
		List<String> eesti = new ArrayList<>();
		eesti.add("Tallinn");
		eesti.add("Tallinn");
		eesti.add("Tallinn");
		eesti.add("Eesti");
		statesWithCities.add(eesti);

		List<String> soome = new ArrayList<>();
		soome.add("Helsinki");
		soome.add("Helsingi");
		soome.add("Helsinki");
		soome.add("Helsingfors");
		soome.add("Soome");
		statesWithCities.add(soome);

		List<String> lati = new ArrayList<>();
		lati.add("Riga");
		lati.add("Riia");
		lati.add("Riga");
		lati.add("Läti");
		statesWithCities.add(lati);

		System.out.println("--Sisestatud andmed-------------");
		System.out.println(statesWithCities);

		// Trükin ainult eesti keelsed nimed
		System.out.println("\n--Eesti keelsed nimed-----------");
		for (List<String> riikLinnadega : statesWithCities) {
			System.out.println(riikLinnadega.get(1));
		}

		System.out.println("\n--Riik on esimeseks liigutatud-----");
		// muudan elementide järjekorda reas, nii et riik läheks esimeseks
		for (List<String> state : statesWithCities) {
			// listi pikkuse järgi võtan viimase elemendi ja sisestan ta
			// esimeseks
			state.add(0, state.get(state.size() - 1));
			// eemaldan viimase elemendi, et see ei jääks korduma
			state.remove(state.size() - 1);
		}
		System.out.println(statesWithCities);

		System.out.println("\n--Riigi nimetuse ette sõna country:----");
		// pannen riigi nimedele ette sõna country:
		for (List<String> state : statesWithCities) {
			state.set(0, "country:" + state.get(0));
		}
		System.out.println(statesWithCities);

		System.out.println("\n--Lausete trükkimine------");
		// trükin "Riik – riik: pealinn – eesti keelne nimi; inglise keeles –
		// inglise keelne nimi, kohalikus keeles – kohalikus keeles nimi"
		for (List<String> state : statesWithCities) {
			// trükin lause kuni kohalike nimedeni
			System.out.print("Riik – " + state.get(0).split(":")[1] + ": pealinn – " + state.get(2)
					+ "; inglise keeles – " + state.get(1) + ", kohalikus keeles – ");
			//lisan kohalikud nimed
			for (int localCityNamePos = 3; localCityNamePos < state.size(); localCityNamePos++) {
				if (localCityNamePos == state.size() - 1) {
					System.out.print(state.get(localCityNamePos));
				} else {
					System.out.print(state.get(localCityNamePos) + "; ");
				}
			}
			System.out.println();
		}

		System.out.println("\n--Uue riigi lisamine kolmandale kohale-----");
		// Loon uue riigi objekti
		List<String> kurrunurru = new ArrayList<>();
		kurrunurru.add("country:Kurrunurruvutisaare Kuningriik");
		kurrunurru.add("LongStocking City");
		kurrunurru.add("Pikksuka linn");
		kurrunurru.add("Langstrump");
		// Lisan kolmandale kohale, lisamisel olemas olevad liiguvad sammu edasi
		statesWithCities.add(2, kurrunurru);
		System.out.println(statesWithCities);

		System.out.println(
				"\n--Kolmandal kohal oleva riigi eemaldamine, nii et tühja rida ei tekiks-----");
		//eemaldan kolmandal kohal oleva riigi, järgnevad liiguvad sammu võrra tagasi
		statesWithCities.remove(2);
		System.out.println(statesWithCities);
		
		//Alternatiivselt oleks saanud öelda, et eemalda kurrunurru, sellisel juhul oleks saanud eemaldada objekti
		//statesWithCities.remove(kurrunurru);
	}

}
