package ee.bcs.koolitus.fifthDay;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CitiesInMap {

	public static void main(String[] args) {

		// valisin kasutamiseks TreeMap-i kuna see hoiab andmeid võtmete järgi
		// järjehoidjas
		// HasMap hoiab andmeid objektidest koostatud hash-ide järgi
		// LinkedHashMap hoiab andmeid nende sisestamise järjekorras
		Map<String, List<String>> statesWithCities = new TreeMap<>();
		List<String> eesti = new ArrayList<>();
		eesti.add("Tallinn");
		eesti.add("Tallinn");
		eesti.add("Tallinn");
		statesWithCities.put("Eesti", eesti);

		List<String> soome = new ArrayList<>();
		soome.add("Helsinki");
		soome.add("Helsingi");
		soome.add("Helsinki");
		soome.add("Helsingfors");
		statesWithCities.put("Soome", soome);

		List<String> lati = new ArrayList<>();
		lati.add("Riga");
		lati.add("Riia");
		lati.add("Riga");
		statesWithCities.put("Läti", lati);

		System.out.println("--Sisestatud andmed-------------");
		System.out.println(statesWithCities);

		// Trükin ainult eesti keelsed nimed
		System.out.println("\n--Eesti keelsed nimed-----------");
		for (Map.Entry<String, List<String>> stateWithCities : statesWithCities.entrySet()) {
			System.out.println(stateWithCities.getValue().get(1));
		}

		// sama asi kasutades lambdasi:
		// statesWithCities.forEach((state, stateCities) -> {
		// System.out.println(stateCities.get(1));
		// });

		// Riigi esimeseks liigutamist ei ole vaja, kuna panin selle võtmeks,
		// alternatiivina oleksin võinud selle lisaks panna ka listi, kuid siis
		// oleks selle liigutamine sama moodi nagu CitiesInList-is

		System.out.println("\n--Riigi nimetuse ette sõna country:----");
		// Võtmete väärtusi ei muudeta, kui selleks vajadus tekib, siis loetakse
		// map-ist võtmele vastav objekt, see eemaldatakse, seejärel lisatakse
		// objekt uue võtmega. Sellise massilise muutmise jaoks tuleks kasutada
		// vaheobjekti konflikti vältimiseks
		Map<String, List<String>> tempMap = new TreeMap<>();
		for (Map.Entry<String, List<String>> stateWithCities : statesWithCities.entrySet()) {
			tempMap.put("country:" + stateWithCities.getKey(), stateWithCities.getValue());
		}
		statesWithCities = tempMap;
		System.out.println(statesWithCities);

		System.out.println("\n--Lausete trükkimine------");
		// trükin "Riik – riik: pealinn – eesti keelne nimi; inglise keeles –
		// inglise keelne nimi, kohalikus keeles – kohalikus keeles nimi"
		for (Map.Entry<String, List<String>> stateWithCities : statesWithCities.entrySet()) {
			List<String> state = stateWithCities.getValue();
			// trükin lause kuni kohalike nimedeni
			System.out.print("Riik – " + stateWithCities.getKey().split(":")[1] + ": pealinn – "
					+ state.get(1) + "; inglise keeles – " + state.get(0)
					+ ", kohalikus keeles – ");
			// lisan kohalikud nimed
			for (int localCityNamePos = 2; localCityNamePos < state.size(); localCityNamePos++) {
				if (localCityNamePos == state.size() - 1) {
					System.out.print(state.get(localCityNamePos));
				} else {
					System.out.print(state.get(localCityNamePos) + "; ");
				}
			}
			System.out.println();
		}

		System.out.println("\n--Uue riigi lisamine kolmandale kohale-----");
		// Map-idega ei saa öelda, mitmendale kohale objekt läheb, seda saab
		// juhtida Map-i tüübi valikuga võtmete kasutamisel. Kuna olen kasutanud
		// sõnalisi võtmeid, lähevad obejktid map-i tähestiku järjekorras, kui
		// oleksin valinud numbrilise, siis numbrite järjekorras. Sama
		// numbrilise võtmega objekti lisamine, oleks tähendanud taas kõigi
		// objektide nihutamist, sarnaselt nagu see sai tehtud massiivides.
		// Loon uue riigi objekti
		List<String> kurrunurru = new ArrayList<>();
		kurrunurru.add("LongStocking City");
		kurrunurru.add("Pikksuka linn");
		kurrunurru.add("Langstrump");
		statesWithCities.put("country:Kurrunurruvutisaare Kuningriik", kurrunurru);
		System.out.println(statesWithCities);

		System.out.println("\n--Lisatud riigi eemaldamine, nii et tühja rida ei tekiks-----");
		// Map-is ei ole ridu sellisel kujul nagu nad on listides ja
		// massiivides, seega ei ole ka tühja rida. Objekti eemaldamine käib
		// tema võtme või võtme ja objekti kaudu kaudu
		statesWithCities.remove("country:Kurrunurruvutisaare Kuningriik");
		System.out.println(statesWithCities);
	}

}
