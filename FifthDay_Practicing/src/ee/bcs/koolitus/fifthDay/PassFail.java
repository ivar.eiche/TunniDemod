package ee.bcs.koolitus.fifthDay;

/**
 * reads from arguments student name and his points for exam. As a result ther
 * will be printed out if student has failed or passed and on latter case, what
 * grade was given If points are less than 51 points, student gets „FAIL“, other
 * cases „PASS - grade, points“ are returned, grade equals 1 = 51-60, 2 = 61-70,
 * 3 = 71-80, 4 = 81-90 and 5= 91-100.
 * 
 * @author heleen
 *
 */
public class PassFail {

	public static void main(String[] args) {
		String name = args[0];
		int points = Integer.parseInt(args[1]);
		if (points < 51) {
			System.out.println(name + " - FAIL");
		} else if (points >= 51 && points < 61) {
			System.out.println(name + " - PASS - 1, " + points);
		} else if (points >= 61 && points < 71) {
			System.out.println(name + " - PASS - 2, " + points);
		} else if (points >= 71 && points < 81) {
			System.out.println(name + " - PASS - 3, " + points);
		} else if (points >= 81 && points < 91) {
			System.out.println(name + " - PASS - 4, " + points);
		} else if (points >= 91) {
			System.out.println(name + " - PASS - 5, " + points);
		}
	}
}
