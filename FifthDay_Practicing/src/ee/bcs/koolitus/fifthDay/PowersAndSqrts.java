package ee.bcs.koolitus.fifthDay;

public class PowersAndSqrts {
	public static void main(String[] args) {
		Double[][] numbersForFormula = { { 10.0, 10.0 }, { 2.0, 4.0 }, { 0.5, 1.5 } };
		for (Double[] numberPair : numbersForFormula) {
			System.out.println("(" + numberPair[0] + "^2 + " + numberPair[1] + "^2)^-2 = "
					+ Math.sqrt(Math.pow(numberPair[0], 2) + Math.pow(numberPair[1], 2)));
		}
	}
}
