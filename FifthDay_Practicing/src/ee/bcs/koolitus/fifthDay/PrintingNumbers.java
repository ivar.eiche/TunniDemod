package ee.bcs.koolitus.fifthDay;

public class PrintingNumbers {

	public static void main(String[] args) {
		int numberToPrint = 1;
		if (numberToPrint == 1) {
			System.out.println(numberToPrint + " = ONE");
		} else if (numberToPrint == 2) {
			System.out.println(numberToPrint + " = TWO");
		} else if (numberToPrint == 3) {
			System.out.println(numberToPrint + " = THREE");
		} else if (numberToPrint == 4) {
			System.out.println(numberToPrint + " = FOUR");
		} else if (numberToPrint == 5) {
			System.out.println(numberToPrint + " = FIVE");
		} else if (numberToPrint == 6) {
			System.out.println(numberToPrint + " = SIX");
		} else if (numberToPrint == 7) {
			System.out.println(numberToPrint + " = SEVEN");
		} else if (numberToPrint == 8) {
			System.out.println(numberToPrint + " = EIGHT");
		} else if (numberToPrint == 9) {
			System.out.println(numberToPrint + " = NINE");
		} else if (numberToPrint == 10) {
			System.out.println(numberToPrint + " = TEN");
		}else {
			System.out.println("Undefined");
		}

		switch (numberToPrint) {
		case 1:
			System.out.println(numberToPrint + " = ONE");
			break;
		case 2:
			System.out.println(numberToPrint + " = TWO");
			break;
		case 3:
			System.out.println(numberToPrint + " = THREE");
			break;
		case 4:
			System.out.println(numberToPrint + " = FOUR");
			break;
		case 5:
			System.out.println(numberToPrint + " = FIVE");
			break;
		case 6:
			System.out.println(numberToPrint + " = SIX");
			break;
		case 7:
			System.out.println(numberToPrint + " = SEVEN");
			break;
		case 8:
			System.out.println(numberToPrint + " = EIGHT");
			break;
		case 9:
			System.out.println(numberToPrint + " = NINE");
			break;
		case 10:
			System.out.println(numberToPrint + " = TEN");
			break;
		default:
			System.out.println("Undefined");
		}

	}

}
