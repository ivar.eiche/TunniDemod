package ee.bcs.koolitus.fourthday.cycle;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

public class ListDemo {

	public static void main(String[] args) {
		Set<String> tekstiList = new TreeSet<>();		
		int i = 0;
		while(i<10) {
			tekstiList.add("Seti Objekt " 
					+ ThreadLocalRandom.current()
						.nextInt(0, 20));
			i++;
		}
		for(String setiElement: tekstiList){
			System.out.println(setiElement);
		}
		
		
	}

}
