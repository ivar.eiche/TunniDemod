package ee.bcs.koolitus.threads;

public class ExampleRunnableSync implements Runnable {
	static int i = 0;

	@Override
	public void run() {
		synchronized (this) {
			i++;
			System.out.println(Thread.currentThread().getName() + " i = " + i);
		}
	}
}
