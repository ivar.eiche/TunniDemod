package ee.bcs.koolitus.employee;

import java.math.BigDecimal;

public class Employee extends Person {
	public static final int MINIMUM_WORKING_AGE = 18;
	private String name;
	private BigDecimal salary;

	private Employee() {
		super(MINIMUM_WORKING_AGE);
	}

	public Employee(int age, String name, BigDecimal salary) {
		super(age);
		this.name = name;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

}
