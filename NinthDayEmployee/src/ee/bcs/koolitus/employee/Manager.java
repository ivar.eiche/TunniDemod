package ee.bcs.koolitus.employee;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {
	private String departement;
	private List<Employee> departementStaff = new ArrayList<>();

	public Manager(int age, String name, BigDecimal salary, String departement) {
		super(age, name, salary);
		this.departement = departement;
	}

	public String getDepartement() {
		return this.departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public void addEmployeeToDepartement(Employee employee) {
		departementStaff.add(employee);
	}

	public List<Employee> getDepartemnetStaff() {
		return this.departementStaff;
	}
}
