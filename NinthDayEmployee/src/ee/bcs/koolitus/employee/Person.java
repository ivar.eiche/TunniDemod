package ee.bcs.koolitus.employee;

public class Person {
	private int age;
	private final int id;
	private static int counter=100;

	public Person(int age) {
		this.id = counter++;
		//counter = counter+1;
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getId(){
		return this.id;
	}
}
