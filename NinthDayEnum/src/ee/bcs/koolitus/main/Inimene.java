package ee.bcs.koolitus.main;

public class Inimene {

	public enum Gender1 {
		MALE, FEMALE
	};

	String nimi;
	Gender1 sugu;

	public Inimene(String nimi, Gender1 sugu) {
		this.nimi = nimi;
		this.sugu = sugu;
	}

}
