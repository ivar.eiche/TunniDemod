package ee.bcs.koolitus.main;

import ee.bcs.koolitus.main.Inimene.Gender1;

public class Main {

	public static void main(String[] args) {
		Inimene inimene = new Inimene("Mario", Gender1.MALE);

		switch (inimene.sugu) {
			case FEMALE:
				System.out.println("See inimene on naine");
				break;
			case MALE:
				System.out.println("See inimene on mees");
				break;
		}
		
		System.out.println(Gender.FEMALE.getDescription());
	}

}
