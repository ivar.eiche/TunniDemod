package ee.bcs.koolitus.secondday;
/**
 * Showing introduction about String-class
 * @author heleen
 *
 */
public class StringExamples {
	public static void main(String[] args) {
		String lause = "isa ütles:\n \"Tule siia!\"";
		System.out.println(lause);
		
		String eesnimi = "Heleen";
		String perenimi = "Maibak";

		String taisNimi = "Heleen maibak";
		String taisNimi2 = eesnimi + " " + perenimi;
		System.out.println(taisNimi == taisNimi2);
		System.out.println(taisNimi.equals(taisNimi2));
		System.out.println(taisNimi.equalsIgnoreCase(taisNimi2));
	}

}
