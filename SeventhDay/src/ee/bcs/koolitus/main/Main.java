package ee.bcs.koolitus.main;

import java.math.BigDecimal;

import ee.bcs.koolitus.employee.Employee;

public class Main {

	public static void main(String[] args) {
		Employee employee1 = new Employee(BigDecimal.valueOf(1_000));
		System.out.println(employee1.getSalary());

		// Employee employee2 = null;
		employee1.changeSalary(employee1);
		System.out.println(employee1.getSalary());

		employee1.changeSalary(employee1, BigDecimal.valueOf(50));
		System.out.println(employee1.getSalary());

		Employee employeeWithName = new Employee("Mari", BigDecimal.valueOf(1200));
		System.out.println(
				employeeWithName.getName() + " has salary " + employeeWithName.getSalary());

		System.out.println(employeeWithName);
	}

}
