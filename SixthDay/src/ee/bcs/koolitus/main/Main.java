package ee.bcs.koolitus.main;

import ee.bcs.koolitus.sixthDay.BeautifulClass;

public class Main {
	public static void main(String[] args){
		BeautifulClass beauty1 = new BeautifulClass("patience", 8);
		beauty1.printBeautyLevel(20);
		
		int calculatedValue = beauty1.calculateBeautyLevel(20);
		System.out.println("Calculated value is " + calculatedValue);
		
		System.out.println(beauty1.getBeautyMeaning());
		beauty1.setBeautyMeaning("patience2");
		System.out.println(beauty1.getBeautyMeaning());
		
		beauty1.setMeasure(10);
	}

//	public static void main(String[] args) {
//		BeautifulClass beauty1 = new BeautifulClass("patience", 10);
//		System.out.println("beauty1 peale loomist: " + beauty1.beautyMeaning + "; " + beauty1.measure);
//		BeautifulClass beauty2 = new BeautifulClass();
//		System.out.println(
//				"beauty1 peale beauty2 loomist: " + beauty1.beautyMeaning + "; " + beauty1.measure);
//		System.out
//				.println("beauty2 peale loomist: " + beauty2.beautyMeaning + "; " + beauty2.measure);
//		BeautifulClass beauty3 = new BeautifulClass(123);
//		System.out.println("beauty1 peale kõigi objektide loomist: " + beauty1.beautyMeaning);
//		System.out.println("beauty2 peale kõigi objektide loomist: " + beauty2.beautyMeaning);
//		System.out.println("beauty3 peale loomist: " + beauty3.beautyMeaning);
//		beauty1.beautyMeaning = "wisdom";
//		System.out.println(beauty1.beautyMeaning);
//		System.out.println(beauty2.beautyMeaning);
//	}

}
