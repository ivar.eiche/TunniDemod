package ee.bcs.koolitus.trycatch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		try (FileReader reader = new FileReader(new File("kasutsajateFail.txt"));
				BufferedReader bfReader = new BufferedReader(reader)) {
			String minuRida;
			List<String> read = new ArrayList<>();
			int counter = 1;
			while ((minuRida = bfReader.readLine()) != null) {
				read.add(minuRida);
				System.out.println(counter + ". " + minuRida);
				counter++;
			}
			System.out.println("Listis on ridu " + read.size());
		} catch (FileNotFoundException e) {
			System.out.println("if you got it, go and have a lunch");
			e.getMessage();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
