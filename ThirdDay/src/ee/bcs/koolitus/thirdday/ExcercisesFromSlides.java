package ee.bcs.koolitus.thirdday;

/**
 * exercises from slides 15 and 17
 * 
 * @author heleen
 *
 */
public class ExcercisesFromSlides {

	public static void main(String[] args) {
		int numberToCheck = Integer.parseInt(args[0]);

		if (numberToCheck % 2 == 0) {
			System.out.println("Number is even");
		} else {
			System.out.println("Number is odd");
		}

		String response = numberToCheck % 2 == 0 ? "Number is even" : "Number is odd";
		System.out.println(response);

		String color = "yellow";
		if (color.equalsIgnoreCase("green")) {
			System.out.println("Driver can drive a car.");
		} else if (color.equalsIgnoreCase("yellow")) {
			System.out.println("Driver has to be ready to stop the car or to start driving.");
		} else if (color.equalsIgnoreCase("red")) {
			System.out.println("Driver has to stop a car and wait for the green light");
		}

		switch (color.toLowerCase()) {
			case "red":
				System.out.println("Driver has to stop a car and wait for the green light");
				break;
			case "yellow":
				System.out.println("Driver has to be ready to stop the car or to start driving.");
				break;
			case "green":
				System.out.println("Driver can drive a car.");
				break;
		}

	}

}
