package ee.bcs.koolitus.thirdday;

public class StringDemoContinues {

	public static void main(String[] args) {
		int i = 7;
		// byte b = i; // translaator annab vea
		byte b = 7;
		int i1 = b; // viga ei ole
		// arve saab teisendada pikemast lühemasse tüüpi
		Integer i2 = 7;
		byte b1 = i2.byteValue(); // OK
		Integer i3 = 777;
		byte b2 = i3.byteValue(); // = 9 
		// 777 = 0x0309 – byteValue võtab viimase baidi
		int i4 = 7;
		// byte b3 = i4.byteValue(); // viga
		// int pole klass ja tal pole meetodeid
		int i5 = 7;
		byte bx = ((Integer) i5).byteValue(); // siin on kasutatud
												// tüübiteisenduse tehet casting

		String neli = "44"; // see on tekst
		int iS = Integer.parseInt(neli); // OK
		System.out.println(iS);
		// iS = Integer.parseInt("tere"); // viga täitmisel
		// String vastus = iS.toString(); // viga - pole klass
		String vastus1 = ((Integer) iS).toString(); // OK
		String vastus2 = Integer.toString(iS);
		System.out.println(vastus1);

		String nimedeLoeteluTekst = "See on loetelu nimedest: Tallinn, Tartu, Pärnu, Kuressaare";
		String[] splitKooloniga = nimedeLoeteluTekst.split(":");
		System.out.println("SplitKooloniga massiivis on " + splitKooloniga.length + " elementi");
		System.out.println("Esimene element on :\"" + splitKooloniga[0] + "\"");
		System.out.println("Teine element on :\"" + splitKooloniga[1].trim() + "\"");
		String[] loeteluNimedest = splitKooloniga[1]
				.split(", ");
		System.out.println("loeteluNimedest massiivis on " 
				+ loeteluNimedest.length + " elementi");
		System.out.println("Esimene element on :\"" 
				+ loeteluNimedest[0].trim() + "\"");
		System.out.println("Teine element on :\"" 
				+ loeteluNimedest[1].trim() + "\"");
		System.out.println("Kolmas element on :\"" + loeteluNimedest[2] + "\"");
		System.out.println("Neljas element on :\"" + loeteluNimedest[3] + "\"");
		
		final int SORMEDE_ARV = 5;
		//SORMEDE_ARV = 5;
		
	}
}
