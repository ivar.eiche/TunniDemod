package ee.bcs.koolitus.thirdday;

public class Tingimuslause {

	public static void main(String[] args) {
		String isikukood1 = "58976456478";
		int esimene = 
				Integer.valueOf(isikukood1.substring(0, 1));
		System.out.println(esimene);
		if(esimene == 4) {
			System.out.println("naine");
		}
		
		if(esimene == 4) {
			System.out.println("naine");
		}else {
			System.out.println("mees");
		}
		
		if(esimene%2 == 0) {
			System.out.println("naine");
		}else if(esimene%2 == 1){
			System.out.println("mees");
		}else {
			System.out.println("tulnukas?");
		}		
		String sugu = esimene%2 == 0 ? "naine" : "mees";
		System.out.println("Sugu = " + sugu);
		
		
		System.out.println("------Switch----");
		switch ("Kati") {
			case "Mati": 
				System.out.println("Mati");	
				break;
			case "Kati":
				System.out.println("Kati");
				break;
			case "Jüri":
				System.out.println("Jüri");
				break;
			default :
				System.out.println("tulnukas");
		}
	}
	
	

}
